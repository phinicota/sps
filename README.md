### deps

* [pixel](github.com/faiface/pixel)

for debian-based
```
sudo apt install libgl1-mesa-dev xorg-dev
```
reference: https://github.com/faiface/pixel/#requirements

### source

* source code:
```
go get -v gitlab.com/phinicota/sps github.com/faiface/glhf github.com/faiface/mainthread github.com/go-gl/glfw/v3.2/glfw github.com/go-gl/mathgl/mgl32 github.com/pkg/errors golang.org/x/image/colornames
```

### run

bash: `go run $(go env GOPATH)/src/gitlab.com/phinicota/sps/main.go`
fish: `go run  (go env GOPATH)/src/gitlab.com/phinicota/sps/main.go`
