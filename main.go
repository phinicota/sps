package main

import (
	_ "image/png"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"gitlab.com/phinicota/sps/models"
	"golang.org/x/image/colornames"
)

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Pixel Rocks!",
		Bounds: pixel.R(0, 0, 1024, 768),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	win.SetSmooth(true)

	universe := models.NewUniverse(win)
	models.GlobalUniverse = universe

	player := models.NewKBPlayer(
		models.NewSpaceship("model1", "model1.png",
			win.Bounds().Center(),
			models.Model1), win)
	player.Controls = models.KBControls2
	universe.NewPlayer(player)

	universe.NewPlayer(
		models.NewKBPlayer(
			models.NewSpaceship("model2", "model2.png",
				win.Bounds().Center().Add(pixel.V(120, 0)), models.Model2), win))

	for !win.Closed() {
		win.Clear(colornames.Black)

		// update states
		universe.Update()

		// draw universe
		universe.Draw()

		// update window
		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}
