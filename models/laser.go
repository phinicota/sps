package models

import (
	"sync"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

const LaserVel = 300

type Laser struct {
	spr        *pixel.Sprite
	pos, vel   pixel.Vec
	Rot        pixel.Matrix
	lastUpdate time.Time
}

func NewLaser(Pos pixel.Vec, Rot pixel.Matrix) *Laser {
	spritesheet, err := loadPicture("laserRed.png")
	if err != nil {
		panic(err)
	}
	return &Laser{
		spr:        pixel.NewSprite(spritesheet, spritesheet.Bounds()),
		vel:        pixel.V(0, LaserVel),
		pos:        Pos,
		Rot:        Rot,
		lastUpdate: time.Now(),
	}
}

func (las *Laser) Pos() pixel.Vec {
	return las.pos
}

func (las *Laser) Draw(win *pixelgl.Window) {
	las.spr.Draw(win, las.Rot.Moved(las.pos))
}

func (las *Laser) Update(win *pixelgl.Window, wg *sync.WaitGroup) {
	dt := time.Since(las.lastUpdate).Seconds()
	defer func() { las.lastUpdate = time.Now() }()
	defer wg.Done()
	las.pos = las.pos.Add(las.Rot.Project(las.vel.Scaled(dt)))

	if !win.Bounds().Contains(las.pos) {
		GlobalUniverse.Destroy(las)
	}
}
