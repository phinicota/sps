package models

import (
	"github.com/faiface/pixel"
)

type EngineFire struct {
	offset pixel.Vec
	On     bool
	anim   *Animation
}

func NewEngineFire(offset pixel.Vec, sprs []*pixel.Sprite) *EngineFire {
	return &EngineFire{
		anim:   NewAnimation(sprs, 0.03),
		offset: offset,
		On:     false,
	}
}

func (eng *EngineFire) Draw(pos pixel.Vec, rot pixel.Matrix, sizeRatio float64,
	target pixel.Target) {
	if eng.On {
		drawPos := pos.Add(rot.Project(eng.offset))
		eng.anim.Draw(drawPos, rot.Scaled(pixel.ZV, sizeRatio), target)
	}
}
