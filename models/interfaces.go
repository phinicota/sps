package models

import (
	"sync"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

type Drawer interface {
	Draw(win *pixelgl.Window)
}

type Updater interface {
	Update(*pixelgl.Window, *sync.WaitGroup)
}

type Poser interface {
	Pos() pixel.Vec
}

type Exploder interface {
	Explode()
}

type Crasher interface {
	Poser
	Exploder
	Crashed(Poser) bool
}

type Object interface {
	Drawer
}
