package models

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

type Explosion struct {
	anim  *Animation
	Pos   pixel.Vec
	ended bool
}

func NewExplosion(pos pixel.Vec) *Explosion {

	spritesheet, err := loadPicture("explosion.png")
	if err != nil {
		panic(err)
	}

	var (
		sprs []*pixel.Sprite
		step = float64(64)
	)
	for y := float64(192); y > spritesheet.Bounds().Min.Y; y -= step {
		for x := float64(0); x < spritesheet.Bounds().Max.X; x += step {
			var (
				fromx = x
				fromy = y
				tox   = x + step
				toy   = y + step
			)
			// fmt.Printf("from: %v, %v -> %v, %v\n", fromx, fromy, tox, toy)
			sprs = append(sprs,
				pixel.NewSprite(spritesheet,
					pixel.R(fromx, fromy,
						tox, toy)))
		}
	}

	return &Explosion{
		anim: NewAnimation(sprs, 0.1),
		Pos:  pos,
	}
}

func (exp *Explosion) Draw(win *pixelgl.Window) {
	exp.anim.Draw(exp.Pos, pixel.IM.Scaled(pixel.ZV, 1.5), win)
	if !exp.ended {
		exp.ended = exp.anim.currentSprInd == len(exp.anim.sprs)-1
	}
}

func (exp *Explosion) Finished() bool {
	return exp.ended
}
