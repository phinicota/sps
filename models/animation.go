package models

import (
	"time"

	"github.com/faiface/pixel"
)

type Animation struct {
	sprs          []*pixel.Sprite
	lastAnim      time.Time
	animSpd       float64
	currentSprInd int
}

func NewAnimation(sprs []*pixel.Sprite, spd float64) *Animation {
	return &Animation{
		sprs:          sprs,
		lastAnim:      time.Now(),
		animSpd:       spd,
		currentSprInd: 0,
	}
}

func (anim *Animation) Draw(pos pixel.Vec, m pixel.Matrix, target pixel.Target) {
	if time.Since(anim.lastAnim).Seconds() > anim.animSpd {
		anim.currentSprInd += 1
		if anim.currentSprInd >= len(anim.sprs) {
			anim.currentSprInd = 1
		}
		anim.lastAnim = time.Now()
	}
	anim.sprs[anim.currentSprInd].Draw(target, m.Moved(pos))
}
