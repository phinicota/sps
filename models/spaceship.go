package models

import (
	"math"
	"time"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

const (
	dragCoef        = 0.006
	angularDragCoef = 0.008
	Model1          = iota
	Model2          = iota
)

type Command struct {
	Moving,
	Shooting,
	RotatingCW,
	RotatingCCW bool
}

type Spaceship struct {
	name string

	// appearance
	basespr                                 *pixel.Sprite
	engineFireSpr                           []*pixel.Sprite
	engFireOffset, wingEngOffset, gunOffset pixel.Vec
	mEngFireSize, thrusterFireSize          float64

	// engines
	leftThruster, rightThruster, mainEngine *EngineFire

	// behaviour
	pos, Vel                                  pixel.Vec
	FAcc, w, angle                            float64
	WAcc                                      float64
	Rot                                       pixel.Matrix
	engineOn, lengineOn, rengineOn, exploding bool
	lastUpdate, lastShoot                     time.Time
	explosion                                 *Explosion

	// specs
	radius          float64
	cadence         float64
	engineAcc, wAcc float64
}

func (sps *Spaceship) Shoot() {
	if time.Since(sps.lastShoot).Seconds() > sps.cadence {
		GlobalUniverse.NewObject(
			NewLaser(sps.pos.Add(sps.Rot.Project(sps.gunOffset)), sps.Rot))

		sps.lastShoot = time.Now()
	}
}

func (sps *Spaceship) AccelCW() {
	sps.leftThruster.On = true
	sps.WAcc = -sps.wAcc
}

func (sps *Spaceship) AccelCCW() {
	sps.rightThruster.On = true
	sps.WAcc = sps.wAcc
}

func (sps *Spaceship) AccelAngZero() {
	sps.rightThruster.On = false
	sps.leftThruster.On = false
	sps.WAcc = 0
}

func (sps *Spaceship) AccelForward() {
	sps.mainEngine.On = true
	sps.FAcc = sps.engineAcc
}

func (sps *Spaceship) AccelForwardZero() {
	sps.mainEngine.On = false
	sps.FAcc = 0
}

func (sps *Spaceship) Update(command Command) {
	if sps.explosion == nil {
		dt := time.Since(sps.lastUpdate).Seconds()
		defer func() { sps.lastUpdate = time.Now() }()

		if command.Moving {
			sps.AccelForward()
		} else {
			sps.AccelForwardZero()
		}

		switch {
		case command.RotatingCCW:
			sps.AccelCCW()
		case command.RotatingCW:
			sps.AccelCW()
		default:
			sps.AccelAngZero()
		}

		if command.Shooting {
			sps.Shoot()
		}

		sps.rotate(dt)
		sps.forward(dt)
	}
}

func (sps *Spaceship) Draw(win *pixelgl.Window) {
	if !sps.exploding {
		sps.basespr.Draw(win, sps.Rot.Moved(sps.pos))
		sps.leftThruster.Draw(sps.pos, sps.Rot, sps.thrusterFireSize, win)
		sps.rightThruster.Draw(sps.pos, sps.Rot, sps.thrusterFireSize, win)
		sps.mainEngine.Draw(sps.pos, sps.Rot, sps.mEngFireSize, win)
	} else {
		if sps.explosion == nil {
			sps.explosion = NewExplosion(sps.pos)
		}
		sps.explosion.Draw(win)
		if sps.explosion.Finished() {
			GlobalUniverse.Destroy(sps)
		}
	}
}

func (sps *Spaceship) Pos() pixel.Vec {
	return sps.pos
}

func (sps *Spaceship) Explode() {
	sps.exploding = true
}

func (sps *Spaceship) Crashed(poser Poser) bool {
	if !sps.exploding {
		x, y := sps.pos.Sub(poser.Pos()).XY()
		if math.Sqrt(math.Pow(x, 2)+math.Pow(y, 2)) < sps.radius {
			sps.Explode()
			return true
		}
	}
	return false
}

func (sps *Spaceship) rotate(dt float64) {
	sps.w += dt*sps.WAcc - sps.w*angularDragCoef
	newangle := sps.angle + sps.w*dt + dt*dt*sps.WAcc
	dangle := newangle - sps.angle
	sps.angle += dangle
	sps.Rot = sps.Rot.Rotated(pixel.ZV, dangle)
}

func (sps *Spaceship) forward(dt float64) {
	acc := sps.Rot.Project(pixel.V(0, dt*sps.FAcc)).Sub(sps.Vel.Scaled(dragCoef))
	sps.Vel = sps.Vel.Add(acc)
	sps.pos = sps.pos.Add(sps.Vel.Scaled(dt))

}

func NewSpaceship(name string, filepath string, startPos pixel.Vec, modelType int) *Spaceship {

	spritesheet, err := loadPicture(filepath)
	if err != nil {
		panic(err)
	}

	var (
		firesprs                               []*pixel.Sprite
		basespr                                *pixel.Sprite
		lengoff, rengoff, mengoff, gunoff      pixel.Vec
		thrusterFireSize, mEngFireSize, radius float64
	)
	switch modelType {
	case Model1:
		radius = 42
		thrusterFireSize = 0.4
		mEngFireSize = 1
		lengoff = pixel.V(-30, -20)
		rengoff = pixel.V(30, -20)
		mengoff = pixel.V(-0.5, -36)
		gunoff = pixel.V(-0.5, 45)
		basespr = pixel.NewSprite(spritesheet, pixel.R(0, 0, 72, 60))
		for step, x := float64(19), float64(72); x < spritesheet.Bounds().Max.X; x += step {
			var (
				fromx = x
				fromy = spritesheet.Bounds().Max.Y - 21
				tox   = x + step
				toy   = spritesheet.Bounds().Max.Y
			)
			// fmt.Printf("from: %v, %v -> %v, %v\n", fromx, 60-fromy, tox, 60-toy)
			firesprs = append(firesprs,
				pixel.NewSprite(spritesheet,
					pixel.R(fromx, fromy,
						tox, toy)))
		}
	case Model2:
		radius = 52
		thrusterFireSize = 0.5
		mEngFireSize = 1.2
		lengoff = pixel.V(-44, -26)
		rengoff = pixel.V(45, -26)
		mengoff = pixel.V(1, -46)
		gunoff = pixel.V(0.5, 55)
		basespr = pixel.NewSprite(spritesheet, pixel.R(0, 0, 98, 74))
		for startx, fireheight, y := float64(199), float64(18), float64(3); y < 4*fireheight; y += fireheight {
			var (
				fromx = startx
				fromy = y
				tox   = spritesheet.Bounds().Max.X
				toy   = y + fireheight
			)
			// fmt.Printf("from: %v, %v -> %v, %v\n", fromx, fromy, tox, toy)
			firesprs = append(firesprs,
				pixel.NewSprite(spritesheet,
					pixel.R(fromx, fromy,
						tox, toy)))
		}
	default:
		panic("choose Model1 or Model2")
	}

	return &Spaceship{
		pos:              startPos,
		Vel:              pixel.ZV,
		Rot:              pixel.IM,
		radius:           radius,
		thrusterFireSize: thrusterFireSize,
		mEngFireSize:     mEngFireSize,
		basespr:          basespr,
		mainEngine:       NewEngineFire(mengoff, firesprs),
		leftThruster:     NewEngineFire(lengoff, firesprs),
		rightThruster:    NewEngineFire(rengoff, firesprs),
		gunOffset:        gunoff,
		engineAcc:        120,
		wAcc:             math.Pi / 2,
		w:                0,
		cadence:          0.8,
		name:             name,
		lastShoot:        time.Now(),
	}
}
