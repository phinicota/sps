package models

import (
	"sync"

	"github.com/faiface/pixel/pixelgl"
)

type PlayerCommand int

const (
	PlayerForward = PlayerCommand(iota)
	PlayerRotCW   = PlayerCommand(iota)
	PlayerRotCCW  = PlayerCommand(iota)
	PlayerShoot   = PlayerCommand(iota)
)

type KBControls map[PlayerCommand]pixelgl.Button

var KBControls1 = KBControls{
	PlayerForward: pixelgl.KeyUp,
	PlayerRotCW:   pixelgl.KeyRight,
	PlayerRotCCW:  pixelgl.KeyLeft,
	PlayerShoot:   pixelgl.KeySpace,
}

var KBControls2 = KBControls{
	PlayerForward: pixelgl.KeyW,
	PlayerRotCW:   pixelgl.KeyD,
	PlayerRotCCW:  pixelgl.KeyA,
	PlayerShoot:   pixelgl.KeyLeftControl,
}

type SpaceshipController interface {
	Moving() bool
	RotatingCW() bool
	RotatingCCW() bool
	Shooting() bool
	HandleEvents(*sync.WaitGroup)
	Spaceship() *Spaceship
}

type Player struct {
	sps *Spaceship
	win *pixelgl.Window
}

func (pl *Player) Spaceship() *Spaceship {
	return pl.sps
}

type KBPlayer struct {
	Player
	Controls KBControls
}

func NewKBPlayer(sps *Spaceship, win *pixelgl.Window) *KBPlayer {
	return &KBPlayer{
		Player:   Player{sps: sps, win: win},
		Controls: KBControls1,
	}
}

func (kbp *KBPlayer) Moving() bool {
	if kbp.win.Pressed(kbp.Controls[PlayerForward]) {
		return true
	}
	return false
}

func (kbp *KBPlayer) Shooting() bool {
	if kbp.win.Pressed(kbp.Controls[PlayerShoot]) {
		return true
	}
	return false
}

func (kbp *KBPlayer) RotatingCCW() bool {

	if kbp.win.Pressed(kbp.Controls[PlayerRotCCW]) {
		return true
	}
	return false
}

func (kbp *KBPlayer) RotatingCW() bool {

	if kbp.win.Pressed(kbp.Controls[PlayerRotCW]) {
		return true
	}
	return false
}

func (kbp *KBPlayer) HandleEvents(wg *sync.WaitGroup) {
	defer wg.Done()
	command := Command{
		Moving:      kbp.Moving(),
		RotatingCW:  kbp.RotatingCW(),
		RotatingCCW: kbp.RotatingCCW(),
		Shooting:    kbp.Shooting(),
	}
	kbp.sps.Update(command)
}

var _ = SpaceshipController(&KBPlayer{})
