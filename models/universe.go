package models

import (
	"sync"

	"github.com/faiface/pixel/pixelgl"
)

var GlobalUniverse *Universe

type Universe struct {
	Objects []Object
	Players []SpaceshipController
	Win     *pixelgl.Window

	mt       sync.Mutex
	toRemove []Object
}

func NewUniverse(win *pixelgl.Window) *Universe {
	return &Universe{
		Objects: []Object{},
		Win:     win,
	}
}

func (univ *Universe) NewObject(obj Object) {
	univ.Objects = append(univ.Objects, obj)
}

func (univ *Universe) NewPlayer(spsc SpaceshipController) {
	univ.Players = append(univ.Players, spsc)
	univ.NewObject(spsc.Spaceship())
}

func (univ *Universe) Destroy(obj Object) {
	univ.mt.Lock()
	defer univ.mt.Unlock()
	univ.toRemove = append(univ.toRemove, obj)
}

func (univ *Universe) Update() {

	var wg sync.WaitGroup
	for _, player := range univ.Players {
		wg.Add(1)
		go player.HandleEvents(&wg)
	}

	for _, object := range univ.Objects {
		if updater, ok := object.(Updater); ok {
			wg.Add(1)
			go updater.Update(univ.Win, &wg)
		}
	}

	wg.Wait()

	for _, object := range univ.Objects {
		if crasher, ok := object.(Crasher); ok {
			for _, otherObj := range univ.Objects {
				if object != otherObj {
					if poser, ok := otherObj.(Poser); ok {
						if crasher.Crashed(poser) {

							// if other object can explode, then BOOM
							if explObj, ok := otherObj.(Exploder); ok {
								explObj.Explode()
							} else {
								// otherwise just quietly disappear
								univ.Destroy(otherObj)
							}
						}
					}
				}
			}
		}
	}

	// destroy objects scheduled to desapear
	for _, obj := range univ.toRemove {
		ind := univ.findObjInd(obj)
		univ.Objects[ind] = nil
		univ.Objects = append(univ.Objects[:ind],
			univ.Objects[ind+1:]...)
	}
	univ.toRemove = nil
}

func (univ *Universe) Draw() {
	for _, object := range univ.Objects {
		object.Draw(univ.Win)
	}
}

func (univ *Universe) findObjInd(obj Object) int {
	for k, object := range univ.Objects {
		if object == obj {
			return k
		}
	}
	panic("wtf obj not found in universe?")
}
