package main

import (
	"fmt"
	"math"

	"github.com/faiface/pixel"
)

func main() {

	fmt.Println("IM:", pixel.IM)
	myvec := pixel.IM.Moved(pixel.V(1, 1))
	fmt.Println("IM.Moved(1,1):", myvec)
	fmt.Println("IM.Rotated(ZV,Pi):", myvec.Rotated(pixel.ZV, math.Pi))
	fmt.Println("IM.Rotated((1,1),Pi):", myvec.Rotated(pixel.V(myvec[4], myvec[5]), math.Pi))
}
